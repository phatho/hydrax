package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"regexp"
	"strconv"

	"github.com/bitly/go-simplejson"
)

func array_key_exit(key string, arr map[string]interface{}) bool {
	for y := range arr {
		if y == key {
			return true
		}
	}
	return false
}

func get_key_hash(hash string) string {
	res, err := http.Get("https://hash.hydrax.net?hash=" + hash)
	if err != nil {
		log.Fatal(err)
	}
	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	ness := string(body[:])
	return ness
}

type DataLink struct {
	Status bool
	Url    string
}

func decode_hydrax(hydrax_key, hydrax_id string) string {

	req, err := http.NewRequest("GET", "https://immortal.hydrax.net/"+hydrax_key+"/"+hydrax_id, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("User-Agent", `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11`)
	req.Header.Set("Origin", "https://hydrax.net")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	//test decode json struct
	var dLink DataLink
	err = json.Unmarshal(body, &dLink)
	if err != nil {
		fmt.Println("There was an error:", err)
	}
	return dLink.Url
}

func getDataHydrax(slug string) map[string]interface{} {

	client := &http.Client{}
	data := url.Values{}
	data.Set("slug", slug)
	res, err := http.NewRequest("POST", "https://multi.hydrax.net/guest",
		bytes.NewBufferString(data.Encode()))
	res.Header.Add("Content-Type", "application/x-www-form-urlencoded; param=value")
	kq, err := client.Do(res)
	if err != nil {
		log.Fatal(err)
	}
	defer kq.Body.Close()

	body, err := ioutil.ReadAll(kq.Body)
	if err != nil {
		log.Fatalln(err)
	}
	js, err := simplejson.NewJson(body)
	arr, err := js.Map()
	if err != nil {
		log.Fatalln(err)
	}
	return arr
}
func pre_match(pattern, ojb string) reflect.Value {

	re := regexp.MustCompile(pattern)
	// fmt.Println(re.Match([]byte(`google.com.vn`)))
	match := re.FindStringSubmatch(ojb)
	return reflect.ValueOf(match)
}

func get_drive_id(url string) string {
	kq := pre_match(`\/([A-Za-z0-9_-]+)\?e=download`, url)
	if kq.Len() != 0 {
		return kq.Index(1).String()
	}
	kq1 := pre_match(`\/files\/([A-Za-z0-9_-]+)`, url)
	if kq1.Index(1).Len() != 0 {
		return kq1.Index(1).String()
	}
	kq2 := pre_match(`files\/([^?]+)`, url)
	if kq2.Index(1).Len() != 0 {
		return kq2.Index(1).String()
	}
	return "no id"
}

// biy5yKYoyj
func main() {

	// os.Exit(1)
	// read input
	// fmt.Print("Enter slug: ")
	// reader := bufio.NewReader(os.Stdin)
	// slug, _ := reader.ReadString('\n')
	// slug = strings.TrimSuffix(slug, "\r\n")
	// json_map := make(map[string]interface{})
	arr := getDataHydrax("biy5yKYoyj")

	for e, m := range arr {
		if e != "sd" {
			continue
		}
		// fmt.Printf("%q", m)
		video_de := m.(map[string]interface{})

		if !array_key_exit("extinf", video_de) {
			continue
		}

		var HLSdata string

		multi_data := video_de["multiData"]
		MultiRange := reflect.ValueOf(video_de["multiRange"])
		extinf := reflect.ValueOf(video_de["extinf"])
		duration := video_de["duration"]
		IV := video_de["iv"]
		HLSdata = fmt.Sprintf("#EXTM3U\n#EXT-X-VERSION:4\n#EXT-X-PLAYLIST-TYPE:VOD\n#EXT-X-TARGETDURATION:%s\n#EXT-X-MEDIA-SEQUENCE:0\n", duration)
		if array_key_exit("hash", video_de) {
			key_hash_req := video_de["hash"].(string)
			key_hash := get_key_hash(key_hash_req)
			c, err := os.Create(e + ".key")
			f, err := c.WriteString(key_hash)
			fmt.Print("tao key thanh cong : %q", f)
			err = c.Close()
			if err != nil {
				fmt.Println(err)
				return
			}
			HLSdata += fmt.Sprintf("#EXT-X-KEY:METHOD=AES-128,URI=\"%s\",IV=%s \n", e+".key", IV)
		}

		s := reflect.ValueOf(multi_data)
		for i := 0; i < s.Len(); i++ {
			yy := s.Index(i)
			uu := yy.Interface()
			tt := uu.(map[string]interface{})
			keyID := strconv.Itoa(i)
			url := decode_hydrax(keyID, tt["file"].(string))
			// fmt.Println(url)
			// xu ly thong tin trong
			ass := MultiRange.Index(i).Interface()
			ttss := reflect.ValueOf(ass)

			for j := 0; j < ttss.Len(); j++ {
				HLSdata += fmt.Sprintf("#EXTINF:%s,\n", extinf.Index(j))
				HLSdata += fmt.Sprintf("#EXT-X-BYTERANGE:%s \n", ttss.Index(j))
				id := get_drive_id(url)
				fmt.Println(id)
				HLSdata += fmt.Sprintf("https://www.googleapis.com/drive/v3/files/%s?key=AIzaSyBMqv4dXnTJOGQtZZS3CBjvf748QvxSzF0&alt=media\n",
					id)
			}

			// break
		}
		HLSdata += "#EXT-X-ENDLIST"
		fmt.Print(HLSdata)
		c, err := os.Create("sd.m3u8")
		f, err := c.WriteString(HLSdata)
		fmt.Print("tao key thanh cong : %q", f)
		err = c.Close()
		if err != nil {
			fmt.Println(err)
			return
		}
	}

}
